package school.student.model;

import lombok.Data;

import javax.persistence.*;
import java.util.Set;

@Entity
@Data
@Table(name = "project")
public class Project {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String projectName;
    /**
     *
     * mappedBy-da qarşı tərəfdəki field-in adı yazılmalıdır.
     * Köhnə value "groups_id" idi, belə field yox idi o biri entity-də, "projects"-ə dəyişdim.
     * Group entity-yə baxsan orda "private Set<Project> projects" görəcəksən,
     * bax o "projects" yazılmalıdır burda
     *
     * */
    @ManyToMany(mappedBy = "projects")

    private Set<Group> groups;
}
