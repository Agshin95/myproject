package school.student.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import school.student.dto.StudentDto;
import school.student.service.StudentService;

@RestController
@RequestMapping("/student")
@RequiredArgsConstructor
public class StudentController {
    private final StudentService studentService;

    @GetMapping("{id}")
    public StudentDto getStudent(@PathVariable Long id) {
        return studentService.getStudent(id);
    }

    @PostMapping
    public StudentDto createStudent(@RequestBody StudentDto dto) {
        return studentService.createStudent(dto);
    }
    @DeleteMapping("{id}")
    public void deleteStudent(@PathVariable Long id) {
        studentService.deletedStudent(id);
    }
    @PutMapping("{id}")
    public StudentDto upDateStudent(@PathVariable Long id, @RequestBody StudentDto dto){
        return studentService.upDateStudent(id, dto);
    }
}
