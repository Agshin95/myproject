package school.student.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import school.student.model.Student;

public interface StudentRepository extends JpaRepository<Student,Long> {
}
