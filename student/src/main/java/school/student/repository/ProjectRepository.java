package school.student.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import school.student.model.Project;

public interface ProjectRepository extends JpaRepository<Project,Long> {
}
