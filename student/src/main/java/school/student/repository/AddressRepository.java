package school.student.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import school.student.model.Address;

public interface AddressRepository extends JpaRepository<Address,Long> {
}
