package school.student.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import school.student.model.Group;

public interface GroupRepository extends JpaRepository<Group, Long> {
}
