package school.student;


import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import school.student.service.StudentService2;

@SpringBootApplication
public class StudentApplication  {
  // private final StudentService2 studentService2;

	public static void main(String[] args) {
		SpringApplication.run(StudentApplication.class, args);

	}

//	@Override
//	public void run(String... args) throws Exception {
//		studentService2.createStudentTable();
//	}
}
