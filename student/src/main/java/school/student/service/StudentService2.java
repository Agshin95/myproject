package school.student.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import school.student.model.Address;
import school.student.model.Student;
import school.student.repository.StudentRepository;
@Service
@RequiredArgsConstructor
public class StudentService2 {
    private final StudentRepository repository;

    public void createStudentTable() {
        Student student= new Student();
        student.setName("Sabuhi");
        student.setLastName("Babayev");
        student.setAge(12);
        student.setPhone("0505891899");
        student.setEmail("Sabuhib@mail.ru");
        //Address
        Address address = new Address();
        address.setCity("baku");
        student.setAddress(address);
        repository.save(student);
    }
}
