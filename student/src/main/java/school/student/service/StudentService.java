package school.student.service;

import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import school.student.dto.StudentDto;
import school.student.model.Student;
import school.student.repository.StudentRepository;

@Service
@RequiredArgsConstructor
public class StudentService {
    private final ModelMapper modelMapper;
    private final StudentRepository repository;

    public StudentDto getStudent(Long id) {
        return repository.findById(id).map((s) -> modelMapper.map(s, StudentDto.class)).
                orElseThrow(() -> new RuntimeException());
    }

    public StudentDto createStudent(StudentDto dto) {
        Student student = modelMapper.map(dto, Student.class);
        Student save = repository.save(student);
        return modelMapper.map(save, StudentDto.class);
    }

    public void deletedStudent(Long id) {
        repository.deleteById(id);
    }

    public StudentDto upDateStudent(Long id, StudentDto dto) {
        Student student = repository.findById(id).orElseThrow(() -> new RuntimeException());
        dto.setId(student.getId());
        Student save = repository.save(modelMapper.map(dto, Student.class));
        return modelMapper.map(save, StudentDto.class);
    }
}
