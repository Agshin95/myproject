package school.student.dto;

import lombok.Data;

import javax.persistence.Column;

@Data
public class StudentDto {
    private  Long id;

    private String name;
    private String lastName;
    private int age;
    private String phone;
    private String email;
}
